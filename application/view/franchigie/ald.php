<p>RCA con massimale di 26.000.000,00 €. Penalità a carico del cliente per sinistro passivo o concorsuale: Euro 250 Capitale infortuni conducente max € 150000 con franchigia 3% su invalidità permanente.</p>
<p>&nbsp;</p>

<h4>Servizi accessori:</h4>
<p>Danni al veicolo/Incendio quota a carico del cliente € 500 per singolo evento. I danni ai cristalli rientrano nella copertura di danni generici al veicolo. La predetta limitazione non opera per i danni causati dal conducente, per qualsiasi motivo, al tetto del veicolo. Furto quota a carico del cliente 10% sul valore commerciale del veicolo al momento dell'evento (eurotax-blu).</p>
