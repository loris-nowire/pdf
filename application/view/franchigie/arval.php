<p>Garanzia assicurativa RCA (penale 250 €). </p>
<p>limitazione di responsabilità per: </p>
<p><img class="arrow" src="<?php echo $assets_url; ?>image/arrow.png"> atti vandalici (penale 500 €); </p>
<p><img class="arrow" src="<?php echo $assets_url; ?>image/arrow.png"> cristalli (penale 150 €); </p>
<p><img class="arrow" src="<?php echo $assets_url; ?>image/arrow.png"> danni accidentali (penale 500 €); </p>
<p><img class="arrow" src="<?php echo $assets_url; ?>image/arrow.png"> eventi naturali (penale 500 €); </p>
<p><img class="arrow" src="<?php echo $assets_url; ?>image/arrow.png"> eventi socio politici (penale 500 €); </p>
<p><img class="arrow" src="<?php echo $assets_url; ?>image/arrow.png"> incendio e furto (penale 500 €). </p>
