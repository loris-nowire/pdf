<page backright="40px" backleft="40px" backtop="100px" class="page-2">

    {# include 'header.php' #}

    <!-- include configurator.php -->

    <div class="features">
        <h4>Garanzie assicurative: <span style="color: <?php echo $custom->color; ?>; font-size: 24px; position: relative; top: 6px">**</span></h4>
        <?php if ($quote->company == 'ald') { ?>
            {# include 'franchigie/ald.php' #}
        <?php } ?>
        <?php if ($quote->company == 'arval') { ?>
            {# include 'franchigie/arval.php' #}
        <?php } ?>
        <?php if ($quote->company == 'leaseplan') { ?>
            {# include 'franchigie/leaseplan.php' #}
        <?php } ?>
        <?php if ($quote->company == 'leasys') { ?>
            {# include 'franchigie/leasys.php' #}
        <?php } ?>

        <h4>Pneumatici:</h4>

        <p class="text-justify">
            Per ciascuna categoria, gli pneumatici verranno sostituiti quando usurati oltre i limiti
            minimi stabiliti dalla norme vigenti ed entro il rispettivo numero sopraindicato per
            tipologia. Le sostituzioni eccedenti il numero stabilito nella singola lettera di offerta
            saranno a carico del Cliente. Da considerare appartenenti alla categoria Premium una
            delle seguenti marche: <strong>Bridgestone</strong>, <strong>Continental</strong>,
            <strong>Dunlop</strong>, <strong>Goodyear</strong>, <strong>Hankook</strong>,
            <strong>Kumho</strong>, <strong>Michelin</strong>, <strong>Nokian</strong>,
            <strong>Pirelli</strong>, <strong>Uniroyal</strong>, <strong>Yokohama</strong> per le
            quali la società di noleggio si riserva la facoltà di scelta.
		</p>
		<p class="text-justify">
			Tutte le altre marche presenti sul mercato
            che siano rispondenti agli standard qualitativi richiesti dalle case automobilistiche
            ed alle norme vigenti in materia di sicurezza, sono da considerarsi appartenenti alla
            categoria Performance (es. <strong>BF-Goodrich</strong>, <strong>Firestone</strong>,
            <strong>Formula</strong>, <strong>Fulda</strong>, <strong>GT_Radial</strong>,
            <strong>Kleber</strong>, <strong>Sava</strong>, <strong>Toyo</strong>).
        </p>

        <div class="condition">
            <p>
                <span style="color: <?php echo $custom->color; ?>; font-size: 24px; position: relative; top: -2px">**</span>
                Garanzie assicurative standard. Modifiche con possibili cambi sul monte canone mese.</p>
        </div>
    </div>

    {# include 'footer.php' #}

</page>
