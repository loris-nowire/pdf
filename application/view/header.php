<page_header>
    <?php if ($quote->commercial) { ?>
    <div class="<?php echo $quote->commercial['class']; ?>">
        <div class="name"><?php echo $quote->commercial['name']; ?></div>
        <div class="telephone"><?php echo $quote->commercial['telephone']; ?></div>
        <div class="email"><?php echo $quote->commercial['email']; ?></div>
        <?php if ($quote->commercial['image']) { ?>
        <img src="<?php echo $quote->commercial['image']; ?>">
        <?php } ?>
    </div>
    <?php } ?>

    <div class="header">
        <p <?php if ($quote->commercial) echo 'style="text-align: left"'; ?>>
            <a href="<?php echo $custom->website; ?>" target="_blank"><img src="<?php echo $custom->logo; ?>"></a>
        </p>
    </div>

    <hr>
</page_header>
