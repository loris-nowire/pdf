<page_footer>
    <hr>
    <div class="footer">
        <p class="first">
            <strong><?php echo $custom->name; ?></strong>
            Tel. <?php echo $custom->telephone; ?> - Fax <?php echo $custom->fax; ?>
            Mail: <a href="<?php echo $custom->email; ?>" target="_blank"><?php echo $custom->email; ?></a>
            Sito Web: <a href="<?php echo $custom->website; ?>" target="_blank"><?php echo $custom->website; ?></a>
        </p>
        <?php if ($is_custom) { ?>
        <p class="second"><?php echo $custom->name; ?> è un marchio di proprietà ed è vietato ogni utilizzo non espressamente autorizzato del marchio e dei suoi contenuti. Partner NoleggioClick.</p>
        <?php } else { ?>
        <p class="second">Noleggioclick è un marchio di proprietà di Nowire è vietato ogni utilizzo non espressamente autorizzato del marchio e dei suoi contenuti.</p>
        <?php } ?>
    </div>
</page_footer>
