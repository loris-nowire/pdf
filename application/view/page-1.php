<page backright="40px" backleft="40px" backtop="80px" class="page-1">

    {# include 'header.php' #}

    <div class="main">
        <table align="center">
            <tr>
                <td class="left">
                    <img src="<?php echo $car->image; ?>">
                    <p class="car-name"><?php echo $car->name; ?></p>
                    <p class="car-detail"><?php echo $quote->detail; ?></p>
                    <p class="subtitle-image">Le immagini sono fornite al solo scopo illustrativo e <br> non costituiscono elemento contrattuale</p>
                </td>
                <td class="right">
                    <table width="100%" align="right" cellpadding="0" cellspacing="0">
                        <tr class="head">
                            <td class="key">Preventivo n°</td>
                            <td class="value"><?php echo $quote->id_quote; ?></td>
                        </tr>
                        <tr>
                            <td class="key">Data</td>
                            <td class="value"><?php echo date('d / m / Y'); ?></td>
                        </tr>
                        <tr>
                            <td class="key">Cliente</td>
                            <td class="value"><?php echo $quote->customer; ?></td>
                        </tr>
                        <?php if ($quote->operator) { ?>
                        <tr>
                            <td class="key">Operatore</td>
                            <td class="value"><?php echo $quote->operator; ?></td>
                        </tr>
                        <?php } ?>
                        <tr>
                            <td class="key">Anticipo</td>
                            <td class="value">€ <?php echo $quote->advance; ?></td>
                        </tr>
                        <tr>
                            <td class="key">Deposito</td>
                            <td class="value">€ <?php echo $quote->storage; ?></td>
                        </tr>
                        <tr>
                            <td class="key">Durata</td>
                            <td class="value"><?php echo $quote->duration; ?> mesi</td>
                        </tr>
                        <tr>
                            <td class="key">Km totali</td>
                            <td class="value"><?php echo $quote->mileage; ?></td>
                        </tr>
                    </table>

                    <p class="car-price">€ <?php echo $quote->price; ?> *</p>
                    <p class="subtitle">Gli importi sono al netto di IVA</p>
                </td>
            </tr>
        </table>
    </div>

    <hr style="margin: 25px 0">

    <p class="title">Tanti servizi in un solo canone</p>

    <table width="100%" class="overview">
        <tr>
            <td class="border">
                <img src="<?php echo $assets_url; ?>image/road.png"><br>
                <p>immatricolazione e<br>messa su strada</p>
            </td>
            <td class="border">
                <img src="<?php echo $assets_url; ?>image/rent.png"><br>
                <p>consegna vettura<br> a domicilio</p>
            </td>
            <td class="border">
                <img src="<?php echo $assets_url; ?>image/insurance.png"><br>
                <p>copertura auto<br>assicurativa totale</p>
            </td>
            <td>
                <img src="<?php echo $assets_url; ?>image/repair.png"><br>
                <p>manutenzione ordinaria<br>e straordinaria</p>
            </td>
        </tr>
        <tr>
            <td colspan="4">&nbsp;<br><br></td>
        </tr>
        <tr>
            <td class="border">
                <img src="<?php echo $assets_url; ?>image/tow.png"><br>
                <p>soccorso stradale<br>in tutta Europa</p>
            </td>
            <td class="border">
                <img src="<?php echo $assets_url; ?>image/signal.png"><br>
                <p>sistemi telematici<br>per sinistri e furti</p>
            </td>
            <td class="border">
                <img src="<?php echo $assets_url; ?>image/wheel.png"><br>
                <p>cambio gomme<br>estive ed invernali</p>
            </td>
            <td>
                <img src="<?php echo $assets_url; ?>image/steering.png"><br>
                <p>auto sostitutiva<br>in caso di necessità</p>
            </td>
        </tr>
    </table>

    <hr style="margin: 28px 0 0">

    <div class="condition">
        <p><span style="color: <?php echo $custom->color; ?>; font-size: 24px; position: relative; top: -2px">*</span> Le dotazioni di serie del veicolo saranno quelle previste dalla casa costruttrice al momento della produzione.
        Questa offerta è valida per un massimo di 30 giorni purché il listino prezzi, le opzioni, le tariffe di assicurazione, i tassi di interesse, gli sconti e le tasse o spese non varino ed è subordinata all'approvazione del merito creditizio. Gli importi sono al netto di IVA. Il pagamento è mensile e anticipato con formula SDD 0gg - driver RID0. Cambio gomme e auto sostitutiva servizi su richiesta.</p>
    </div>

    {# include 'footer.php' #}

</page>
