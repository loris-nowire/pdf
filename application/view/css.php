<style type="text/css">

p {
    line-height: 30px;
}

.features h4 {
    margin: 30px 0 20px;
}

.features p {
    margin: 0;
}

.text-justify {
    text-align: justify;
}

.arrow {
    height: 14px;
    margin-right: 10px;
    margin-left: 10px;
}

.header p {
    margin: 10px 0 0;
    text-align: center;
}

hr {
    margin: 10px 0;
    border: 0.25px solid #AAA;
}

.commercial {
    position: absolute;
    top: 10px;
    right: 0;
    color: #666;
    font-size: 12px;
}

.commercial .name {
    position: absolute;
    top: 1px;
    right: 75px;
    font-weight: bold;
}

.commercial .telephone {
    position: absolute;
    top: 21px;
    right: 75px;
}

.commercial .email {
    position: absolute;
    top: 41px;
    right: 75px;
}

.commercial img {
    width: 60px;
    border-radius: 5mm;
    -moz-border-radius: 5mm;
    -webkit-border-radius: 5mm;
}

.commercial-no-image {
    position: absolute;
    top: 10px;
    right: 0px;
    color: #666;
    font-size: 12px;
}

.commercial-no-image .name {
    position: absolute;
    top: 1px;
    right: 0;
    font-weight: bold;
}

.commercial-no-image .telephone {
    position: absolute;
    top: 21px;
    right: 0;
}

.commercial-no-image .email {
    position: absolute;
    top: 41px;
    right: 0;
}

.title {
    text-align: center;
    font-size: 28px;
    margin: 30px 0;
    font-weight: bold;
}

.main {
    margin: 20px 0 0;
}

.main hr {
    margin: 20px 0;
}

.main td.left {
    text-align: center;
    width: 360px;
}

.main td.right {
    width: 340px;
    vertical-align: middle;
}

.main td.right table td {
    border: 0;
    border-bottom: 0.5px solid #AAA;
    padding: 8px 10px;
    font-size: 12px;
}

.main td.right table .key {
    width: 80px;
    padding-right: 0;
}

.main td.right table .value {
    width: 200px;
    padding-left: 0;
}

.main td.right table tr.head {
    background: #EEE;
}

.main td.right table .key {
    text-align: left;
    font-weight: bold;
}

.main td.right table .value {
    text-align: right;
}

.main td.center {
    width: 40px;
}

.main .car-name {
    margin: 0;
    width: 20px;
}

.main .car-name {
    line-height: 20px;
    margin: 0;
    font-size: 18px;
    color: #000;
    font-weight: bold;
}
.main .car-detail {
    font-size: 14px;
    margin-bottom: 0;
    color: #000;
}

.main .car-price {
    margin: 20px 0 5px;
    font-size: 30px;
    color: <?php echo $custom->color; ?>;
    font-weight: bold;
    text-align: center;
}

.main .subtitle {
    text-align: center;
    margin: 5px 0 0;
    font-size: 11px;
    color: #999;
}

.main .subtitle-image {
    text-align: center;
    margin: 5px 0 0;
    font-size: 11px;
    color: #999;
    line-height: 20px;
}

.main .center {
    text-align: center;
    margin: 0 auto;
}

.main .margin-30 {
    margin: 40px 0;
}

.overview {
    margin: 0;
}

.overview td {
    text-align: center;
    width: 163px;
}

.overview .border {
    border-right: 0.25px solid #AAA;
}

.overview img {
    max-width: 67px;
    max-height: 67px;
}

.overview p {
    margin-top: 30px;
    font-size: 11pt;
    line-height: 24px;
}

.condition {
    margin: 15px 20px 0;
    font-size: 11px;
    text-align: center;
    color: #666;
}

.condition-bottom {
    margin: 10px 40px 0;
    font-size: 12px;
    text-align: justify;
    color: #666;
}

.condition p {
    margin: 4px 0;
    line-height: 20px;
}

.footer p.first {
    text-align: center;
    font-size: 11px;
    line-height: 14px;
    color: #333;
    margin: 10px 0 5px;
}

.footer p.second {
    text-align: center;
    font-size: 9px;
    line-height: 14px;
    color: #999;
    margin: 5px 0;
}

.footer a {
    text-decoration: none;
    font-weight: bold;
    color: <?php echo $custom->color; ?>;
}

.footer td.first {
    width: 320px;
}

.footer td.second {
    width: 320px;
}

.footer .text-right {
    text-align: right;
}

.footer img {
    margin: 0 0 12px;
    width: 250px;
}

</style>
