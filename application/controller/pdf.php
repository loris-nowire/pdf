<?php

    use Spipu\Html2Pdf\Html2Pdf;
    use Spipu\Html2Pdf\Exception\Html2PdfException;

    class Pdf extends Crystal {

        public function __construct() {

            $this->normalize  = $this->app('class', 'normalize');
            $this->agency     = $this->app('model', 'agency');
            $this->car        = $this->app('model', 'car');
            $this->commercial = $this->app('model', 'commercial');
            $this->quote      = $this->app('model', 'quote');
            $this->config     = $this->app('core', 'config');
            $this->file       = $this->app('core', 'file');
            $this->image      = $this->app('core', 'image');
            $this->template   = $this->app('core', 'template');
        }

        public function index() { }

        // $type = F for save, I for view, D for download
        public function generate($id_quote, $filename = null) {

            $quote = $this->quote->get($id_quote);
            $quote = $this->normalize->quote($quote);
            $car   = $this->car->get($quote->id_car);
            $car   = $this->normalize->car($car);

            $data = [
                'quote'  => $quote,
                'car'    => $car,
                'custom' => $this->custom($quote)
            ];

            $html     = $this->template->output('template', $data);
            $filename = $filename ? $filename . '.pdf' : 'NoleggioClick-' . $car->alias . '-' . $quote->id_quote . '.pdf';
            $filepath = $this->config->get('uploads_path') . 'quote/' . $filename;

            mkdir(dirname($filepath), 0664);

            ob_clean();

            try {
                $html2pdf = new Html2Pdf('P', 'A4', 'it');
                $html2pdf->pdf->SetAuthor('NoleggioClick');
                $html2pdf->pdf->SetTitle('Preventivo n° ' . $quote->id_quote);
                $html2pdf->pdf->SetSubject('Preventivo');
                $html2pdf->pdf->SetDisplayMode('fullpage');
                $html2pdf->writeHTML($html);
                $html2pdf->output($filepath, 'F');
            } catch (Html2PdfException $e) {
                var_dump($e); die;
            }

            $results = [
                'success'  => is_file($filepath),
                'filename' => $filename
            ];

            echo json_encode($results, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
        }

        protected function custom($quote) {

            $id_noleggioclick = 1;
            $agency = $this->agency->get($id_noleggioclick);

            $custom = (object)[
                'name'      => '<span style="color: #666">Noleggio</span><span style="color: ' . $agency->color . '">Click</span>',
                'logo'      => $this->config->get('uploads_url') . 'agency/' . $agency->id_agency . '.png',
                'telephone' => $agency->telephone,
                'fax'       => $agency->fax,
                'email'     => $agency->email,
                'website'   => $agency->website,
                'color'     => $agency->color
            ];

            if ($quote->id_commercial) {
                $commercial = $this->commercial->get($quote->id_commercial);
                $agency     = $this->agency->get($commercial->id_agency);

                if ($agency->id_agency != $id_noleggioclick && $agency->pdf) {
                    $custom = (object)[
                        'name'      => $agency->name,
                        'logo'      => $this->config->get('uploads_url') . 'agency/' . $agency->id_agency . '.png',
                        'telephone' => $agency->telephone,
                        'fax'       => $agency->fax,
                        'email'     => $agency->email,
                        'website'   => $agency->website,
                        'color'     => $agency->color,
                    ];
                }
            }

            return $custom;
        }
    }
