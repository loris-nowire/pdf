<?php

    class Operator_Model extends Crystal {

        public function get($id_operator) {

            $this->db->select('*');
            $this->db->from('operator');
            $this->db->where('id_operator', '=', $id_operator);

            return $this->db->row();
        }
    }
