<?php

    class Quote_Model extends Crystal {

        public function get($id_quote) {

            $this->db->select('*');
            $this->db->from('quote');

            $this->db->where('id_quote', '=', $id_quote);

            return $this->db->row();
        }
    }
