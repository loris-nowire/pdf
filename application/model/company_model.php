<?php

    class Company_Model extends Crystal {

        public function get($id_company) {

            $this->db->select('*');
            $this->db->from('company');

            $this->db->where('id_company', '=', $id_company);

            return $this->db->row();
        }
    }
