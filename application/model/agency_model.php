<?php

    class Agency_Model extends Crystal {

        public function get($id) {

            $this->db->select('*');
            $this->db->from('agency');
            $this->db->where('id_agency', '=', $id);

            return $this->db->row();
        }
    }
