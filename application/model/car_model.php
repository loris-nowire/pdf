<?php

    class Car_Model extends Crystal {

        public function get($id_car) {

            $this->db->select('car.*', 'manufacturer.name AS manufacturer', 'type.name AS type', 'fuel.name AS fuel');
            $this->db->from('car');
            $this->db->join('manufacturer', 'car', 'id_manufacturer');
            $this->db->join('type', 'car', 'id_type');
            $this->db->join('fuel', 'car', 'id_fuel');
            $this->db->where('car.id_car', '=', $id_car);

            return $this->db->row();
        }
    }
