<?php

    class Normalize extends Crystal {

        public function __construct() {

            $this->commercial = $this->app('model', 'commercial');
            $this->car        = $this->app('model', 'car');
            $this->company    = $this->app('model', 'company');
            $this->operator   = $this->app('model', 'operator');
            $this->config     = $this->app('core', 'config');
            $this->image      = $this->app('core', 'image');
        }

        public function quote($row) {

            if (!$row->id_quote) {
                $this->error('Il preventivo non esiste');
            }

            $car = $this->car->get($row->id_car);
            $car = $this->car($car);

            if ($row->id_commercial) {
                $elem = $this->commercial->get($row->id_commercial);

                $elem->image = $this->config->get('uploads_path') . 'commercial/' . $row->id_commercial . '.png';
                $elem->image = file_exists($elem->image) ? $this->config->get('uploads_url') . 'commercial/' . $elem->id_commercial . '.png' : null;

                $commercial = [
                    'name'      => $elem->name . ' ' . $elem->surname,
                    'telephone' => $elem->telephone,
                    'email'     => $elem->email,
                    'image'     => $elem->image,
                    'class'     => $elem->image ? 'commercial' : 'commercial-no-image'
                ];
            }

            if ($row->id_operator) {
                $operator = $this->operator->get($row->id_operator);
                $operator = $operator->username;
            }

            if ($row->id_company) {
                $company = $this->company->get($row->id_company);
                $company = $company->alias;
            }

            $data = (object)[
                'id_quote'      => $row->id_quote,
                'id_car'        => $row->id_car,
                'id_agency'     => $row->id_agency,
                'id_commercial' => $row->id_commercial,
                'commercial'    => $commercial,
                'operator'      => $operator,
                'customer'      => $row->customer,
                'car'           => $car->name,
                'detail'        => $row->detail,
                'price'         => number_format($row->price, 2, ',', '.'),
                'advance'       => number_format($row->advance, 0, ',', '.'),
                'storage'       => number_format($row->storage, 0, ',', '.'),
                'mileage'       => number_format($row->mileage, 0, ',', '.'),
                'duration'      => $row->duration,
                'image'         => $car->image,
                'company'       => $company
            ];

            return $data;
        }

        public function car($row) {

            if (!$row->id_car) {
                $this->error('L\'auto non esiste');
            }

            $image        = $this->config->get('uploads_url') . 'car/' . $row->alias . '/' . $row->alias . '-noleggio-lungo-termine-1.jpg';
            $thumbnail    = 'thumbs/car-' . $row->alias . '.jpg';
            $uploads_path = $this->config->get('uploads_path');
            $uploads_url  = $this->config->get('uploads_url');

            $this->image->load($image);
            $this->image->best_fit(360, 280);
            $this->image->save($uploads_path . $thumbnail, 80, 'jpg');

            if (!file_exists($uploads_path . $thumbnail)) {
                $this->error('L\'immagine dell\'auto non esiste');
            }

            $data = (object)[
                'id_car'       => $row->id_car,
                'manufacturer' => $row->manufacturer,
                'model'        => $row->model,
                'name'         => $row->manufacturer . ' ' . $row->model,
                'type'         => $row->type,
                'fuel'         => $row->fuel,
                'year'         => $row->year,
                'alias'        => $row->alias,
                'price'        => number_format($row->price, 0, ',', ''),
                'advance'      => $row->advance ? number_format($row->advance, 0, ',', '') : 0,
                'duration'     => $row->duration ? number_format($row->duration, 0, ',', '') : 0,
                'mileage'      => $row->mileage ? number_format($row->mileage, 0, ',', '') : 0,
                'image'        => $uploads_url . $thumbnail,
            ];

            return $data;
        }

        protected function error($string) {

            header("Content-type: application/json; charset=utf-8");
            echo json_encode(['error' => $string]);
            die;
        }
    }
