<?php

    // General Config

        $initial = 'local';

    // Local Config

        $config['local'] = [
            'masterconfig'     => '../masterconfig.php',
            'system_path'      => '../../crystal/',
            'application_path' => 'application/',
            'assets_path'      => 'application/view/assets/',
            'uploads_path'     => '../upload/',
            'vendors_path'     => '../vendor/',
            'seo_friendly'     => false,
            'minimize_html'    => false,
            'routing'          => false,
            'debug'            => true,
            'profiler'         => false,
            'error_reporting'  => E_ALL & ~ E_NOTICE & ~ E_WARNING,
            'error_page'       => 'pdf/error',
            'log_file'         => 'debug.txt',
            'start'            => 'Pdf',
            'language'         => 'it'
        ];
